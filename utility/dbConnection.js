const mysql = require('mysql2');

// const pool = mysql.createPool({
//     host: 'localhost',
//     port:'3307',
//     user: 'root',
//     password: 'root',
//     database: 'marketplaze'
    
// });

const pool = mysql.createPool({
    host: 'docker_mysql',
    port:'3306',
    user: 'root',
    password: 'root',
    database: 'marketplaze'
    
});


module.exports = pool;