
checkOutHelper = (quantity, promo, unitPrice, minBuy, freeItem) => {
    //will return subtotal, discount, total
    if(promo == '-' || promo == ''){

        let itemSubtotal = quantity*unitPrice;
        let itemDiscount = 0;
        let itemTotal = itemSubtotal - itemDiscount;

        return {
            subtotal : itemSubtotal,
            discount : itemDiscount,
            total : itemTotal

        };
    }
    
    if(quantity < minBuy){
        let itemSubtotal = quantity*unitPrice;
        let itemDiscount = 0;
        let itemTotal = itemSubtotal - itemDiscount;
    
        return {
            subtotal : itemSubtotal,
            discount : itemDiscount,
            total : itemTotal
            };
        }

    else if(quantity > minBuy){

        let X = Math.floor( quantity / (minBuy + freeItem) );
            let total1 = X * unitPrice;

        let Y = quantity % (minBuy + freeItem);
            let total2 = Y * unitPrice;

            let itemSubtotal = quantity*unitPrice;
            let itemTotal = itemSubtotal - (total1 + total2);
            let itemDiscount = itemSubtotal - itemTotal;
            

        return {
            subtotal : itemSubtotal,
            discount : itemDiscount,
            total : itemTotal
        }

    }


        
}

module.exports= checkOutHelper