const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const corsMiddleware = require('./utility/CORSmiddleware')
const productRouter = require ('./routes/productRouter')
const orderRouter = require ('./routes/customerOrderRouter')
const orderItemRouter = require ('./routes/orderItemRouter')

app.use(bodyParser.json())

app.use(corsMiddleware)
app.use('/marketplaze/api', productRouter);
app.use('/marketplaze/api', orderRouter);
app.use('/marketplaze/api', orderItemRouter)


app.get('/', (req,res)=> {
    res.send('marketplaze landing page')
})

app.listen(3000);


