const pool = require('../utility/dbConnection')

getAllOrderItemByUserId = (userId) => {
    let sql = 'SELECT * FROM marketplaze.order_item '+
    "WHERE user_id = ?;"
    return new Promise((resolve, reject)=>{
        pool.query(sql,[userId],  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    });
}

createOrderItemByUserId = (customerOrderId, userId, productId, quantity, promoType, subtotal, discount, total) =>{
    let sql ='INSERT INTO order_item (customer_order_id, user_id, product_id, quantity, promo_type, subtotal, discount, total ) VALUES (?, ?, ?, ?, ?, ?, ?, ?);'
    return new Promise((resolve, reject)=>{
        pool.query(sql,[customerOrderId, userId, productId, quantity, promoType, subtotal, discount, total],  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    });
}

getAllOrderItemByOrderId =(customerOrderId, userId) => {
    let sql ='SELECT customer_order_id, user_id, order_item.product_id, product.name, unit_price, quantity,promo_type, subtotal, discount, total FROM marketplaze.order_item '+
            'JOIN marketplaze.product '+
            'ON product.product_id = order_item.product_id '+
            'WHERE customer_order_id = ? AND user_id = ? ;'

    return new Promise((resolve, reject)=>{
        pool.query(sql,[customerOrderId, userId],  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    });

        
}

   
exports.getAllOrderItemByUserId = getAllOrderItemByUserId;
exports.createOrderItemByUserId = createOrderItemByUserId;
exports.getAllOrderItemByOrderId= getAllOrderItemByOrderId;