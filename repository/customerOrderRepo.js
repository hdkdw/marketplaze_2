const pool = require('../utility/dbConnection')

getAllOrderByUserId = (userId) => {
    let sql = 'SELECT * FROM marketplaze.customer_order '+
    "WHERE user_id = ?;"
    return new Promise((resolve, reject)=>{
        pool.query(sql,[userId],  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    });
}

createOrderByUserId = (userId) =>{
    let sql ='INSERT INTO customer_order (user_id) VALUES (?)'
    return new Promise((resolve, reject)=>{
        pool.query(sql,[userId],  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    });
}

getCheckOutSummary = (customerOrderId, userId)=>{

    let sql ='SELECT customer_order.customer_order_id, customer_order.user_id, SUM(order_item.quantity) AS TotalItem, '+
            'SUM(order_item.subtotal) AS Subtotal, SUM(order_item.discount) AS Discount, SUM(order_item.total) AS GrandTotal FROM marketplaze.customer_order '+
            'JOIN marketplaze.order_item '+
            'ON customer_order.customer_order_id = order_item.customer_order_id '+
            'WHERE customer_order.customer_order_id = ? AND customer_order.user_id =? '+
            'GROUP BY customer_order.customer_order_id; '

    return new Promise((resolve, reject)=>{
        pool.query(sql,[customerOrderId,userId],  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    });

}

createCheckout = (totalQuantity,subtotal, discount, grandTotal,customerOrderId, userId) => {

    let sql = 'UPDATE customer_order SET total_quantity = ?, subtotal= ?, discount=?, grand_total = ? '+
            'WHERE customer_order_id = ? AND user_id=?;'

    return new Promise((resolve, reject)=>{
        pool.query(sql,[totalQuantity,subtotal, discount, grandTotal,customerOrderId, userId],  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    });


}


exports.getAllOrderByUserId = getAllOrderByUserId;
exports.createOrderByUserId = createOrderByUserId;
exports.getCheckOutSummary= getCheckOutSummary;
exports.createCheckout = createCheckout;