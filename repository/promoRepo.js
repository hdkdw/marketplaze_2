const pool = require('../utility/dbConnection')

getPromoType = (productId) => {
    let sql = "SELECT CASE WHEN promo_type IS NOT NULL THEN (CONCAT('Beli ',min_buy,' Gratis ',free_item))"+
    "ELSE '-' END AS promo ,"+
    'min_buy, free_item, unit_price '+
    'FROM promo '+
    'RIGHT JOIN product ON product.product_id = promo.product_id '+
    'WHERE promo.product_id = ?'
    return new Promise((resolve, reject)=>{
        pool.query(sql,[productId],  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    });
}

checkProduct = (productId) => {
    let sql = 'SELECT EXISTS(SELECT product_id from promo WHERE product_id =?) AS checks;'
    return new Promise((resolve, reject)=>{
        pool.query(sql,[productId],  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    });

}


exports.getPromoType = getPromoType;
exports.checkProduct = checkProduct;