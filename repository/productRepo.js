const pool = require('../utility/dbConnection')


getAllProduct = () => {
    let sql = 'SELECT product.product_id, product.name, unit_price,'+
    "CASE WHEN promo_type IS NOT NULL THEN (CONCAT('Beli ',min_buy,' Gratis ',free_item))"+
    "ELSE '-' END AS promo "+
    'FROM promo '+
    'RIGHT JOIN product ON product.product_id = promo.product_id;'
    return new Promise((resolve, reject)=>{
        pool.query(sql,  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
        });
    });
}


getOnlyProductWithPromo = () =>{
    let sql = 'SELECT product.product_id, product.name, unit_price,'+
    "CASE WHEN promo_type IS NOT NULL THEN (CONCAT('Beli ',min_buy,' Gratis ',free_item))"+
    "ELSE '-' END AS promo "+
    'FROM promo '+
    'JOIN product ON product.product_id = promo.product_id '+
    'WHERE promo_type IS NOT NULL'

    return new Promise((resolve, reject)=>{
        pool.query(sql,  (error, elements)=>{
            if(error){
                return reject(error);
            }
            return resolve(elements);
            });
        });
    };
    

    getProductIdPriceData = (productId) =>{
        let sql = 'SELECT unit_price FROM product WHERE product_id = ?;'
        return new Promise((resolve, reject)=>{
            pool.query(sql, [productId], (error, elements)=>{
                if(error){
                    return reject(error);
                }
                return resolve(elements);
                });
            });
        };


exports.getAllProduct = getAllProduct;
exports.getOnlyProductWithPromo = getOnlyProductWithPromo;
exports.getProductIdPriceData = getProductIdPriceData;
