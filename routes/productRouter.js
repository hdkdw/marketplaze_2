var express = require('express');
var router = express.Router();

const ProductService = require('../services/productServices')
const productServices = new ProductService

router.get('/allproduct', productServices.getAllProduct);
router.get('/productwithpromo', productServices.getOnlyProductWithPromo);



module.exports = router;