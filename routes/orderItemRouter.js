var express = require('express');
var router = express.Router();

const OrderItemService = require('../services/orderItemService')
const orderItemServices = new OrderItemService

router.get('/orderItembyUserId', orderItemServices.getAllOrderItemByUserId);
router.post('/createOrderItemByUserId', orderItemServices.createOrderItemByUserId);
router.get('/allOrderItemByOrderId', orderItemServices.getAllOrderItemByOrderId2);

module.exports = router;